#include "Player.h"


Player::Player(float x, float y) : Entity(x, y)
{
	_lives = 3;
	_speed = 150.0f;
	startingPosition.x = x;
	startingPosition.y = y;
}
int Player::getLives() const
{
	return _lives;
}

void Player::setLives(int lives)
{
	_lives = lives;
}

void Player::MoveLeft(sf::Time* elapsed)
{
	_sprite.move(-_speed * (*elapsed).asSeconds(), 0);
}

void Player::MoveRight(sf::Time* elapsed)
{
	_sprite.move(_speed * (*elapsed).asSeconds(), 0);
}

void Player::MoveUp(sf::Time* elapsed)
{
	_sprite.move(0, -_speed * (*elapsed).asSeconds());
}

void Player::MoveDown(sf::Time* elapsed)
{
	_sprite.move(0, _speed * (*elapsed).asSeconds());
}