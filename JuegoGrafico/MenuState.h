#ifndef MENUSTATE_H
#define MENUSTATE_H
#include "State.h"

class MenuState : public State
{
private:
	sf::Font _font;
	sf::Text _textGame;
	sf::Text _textPlay;
	sf::Text _textCredits;
public:
	MenuState();
	virtual void UpdateFrame(sf::RenderWindow* window, sf::Time* elapsed);
};
#endif // !MENUSTATE_H