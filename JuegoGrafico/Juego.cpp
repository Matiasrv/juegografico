#include "Juego.h"

Juego::Juego()
{
	currentState = nullptr;//no se si esto sirva de algo, nullptr creo que era un puntero a null
	std::string windowName;
#if DEBUG
	windowName = "Debug Window";
#elif RELEASE 
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	windowName = "Release Window";	//Cambiar a nombre de juego version final
#endif

	srand(time(NULL));

	_window.create(sf::VideoMode(_windowWidth, _windowHeight), windowName);
	_window.setFramerateLimit(_framesPerSecond);
}

Juego::~Juego()
{
	_mainMusic.stop();
}

void Juego::Run()
{
	currentState = new MenuState();

	_mainMusic.openFromFile("Assets/Audio/music.ogg");

	_mainMusic.setLoop(true);
	_mainMusic.play();

	while (_window.isOpen())	// run the program as long as the window is open
	{
		sf::Time elapsedTime = _clock.restart(); // tiempo transcurrido entre el frame anterior y el actual

												// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (_window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				_window.close();
		}
		_window.clear(sf::Color::White);		// clear the window with white color

		// draw everything here... window.draw(...);
		//--------------------------------frame start---------------------------------

		StateManager();


		if (currentState)//
			currentState->UpdateFrame(&_window, &elapsedTime);

		//---------------------------------frame end----------------------------------
		_window.display();
	}
	if (currentState)
		delete currentState;
}

void Juego::StateManager()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
	{
		if (currentState->GetCurrentState() == "inGame")
		{
			delete currentState;
			currentState = new InGameState();
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		if (currentState->GetCurrentState() == "Menu")
		{
			delete currentState;
			currentState = new InGameState();
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		if (currentState->GetCurrentState() == "inGame")
		{
			delete currentState;
			currentState = new MenuState();
		}

		if (currentState->GetCurrentState() == "Credits")
		{
			delete currentState;
			currentState = new MenuState();
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
	{
		if (currentState->GetCurrentState() == "Menu")
		{
			delete currentState;
			currentState = new CreditState();
		}
	}
}
