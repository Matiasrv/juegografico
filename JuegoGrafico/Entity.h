#ifndef ENTITY
#define ENTITY
#include "GameObject.h"

class Entity : public GameObject
{
protected:
	int _lives;
	float _speed;
public:
	Entity(float x, float y);
	void setLives(int lives);
	int getLives() const;
	virtual void MoveLeft(sf::Time* elapsed)=0;
	virtual void MoveRight(sf::Time* elapsed)=0;
	virtual void MoveUp(sf::Time* elapsed)=0;
	virtual void MoveDown(sf::Time* elapsed)=0;
};

#endif // !ENTITY