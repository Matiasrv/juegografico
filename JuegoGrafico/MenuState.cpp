#include "MenuState.h"

MenuState::MenuState()
{
	_nameState = "Menu";

	_font.loadFromFile("Assets/Fonts/orange juice.ttf");

	_textGame.setFont(_font);
	_textGame.setString("Game");
	_textGame.setCharacterSize(48);
	_textGame.setFillColor(sf::Color::Red);
	_textGame.setStyle(sf::Text::Bold | sf::Text::Underlined);

	_textPlay.setFont(_font);
	_textPlay.setString("		Press Enter To Play\n And BackSpace To Reset Level");
	_textPlay.setCharacterSize(26);
	_textPlay.setFillColor(sf::Color::Black);

	_textCredits.setFont(_font);
	_textCredits.setString("		 Press C To See Credits\n\nPress Escape To Return To Menu");
	_textCredits.setCharacterSize(26);
	_textCredits.setFillColor(sf::Color::Blue);
}

void MenuState::UpdateFrame(sf::RenderWindow * window, sf::Time * elapsed)
{
	sf::Vector2u windowSize = window->getSize();
	_textGame.setPosition(windowSize.x / 2 - _textGame.getGlobalBounds().width / 2 - (rand() % 3), windowSize.y / 5 - _textGame.getGlobalBounds().height / 2 - (rand() % 3));
	window->draw(_textGame);

	_textPlay.setPosition(windowSize.x / 2 - _textPlay.getGlobalBounds().width / 2 - (rand() % 3), windowSize.y / 3 - _textPlay.getGlobalBounds().height / 2 - (rand() % 3));
	window->draw(_textPlay);

	_textCredits.setPosition(windowSize.x / 2 - _textCredits.getGlobalBounds().width / 2 - (rand() % 3), windowSize.y / 2 - _textCredits.getGlobalBounds().height / 2 - (rand() % 3));
	window->draw(_textCredits);
}
