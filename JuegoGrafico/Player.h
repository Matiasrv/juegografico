#ifndef PLAYER_H
#define PLAYER_H
#include "Entity.h"

class Player : public Entity
{
public:
	Player(float x, float y);
	int getLives() const;
	void setLives(int lives);
	virtual void MoveLeft(sf::Time* elapsed);
	virtual void MoveRight(sf::Time* elapsed);
	virtual void MoveUp(sf::Time* elapsed);
	virtual void MoveDown(sf::Time* elapsed);
	sf::Vector2f startingPosition;
};
#endif // !PLAYER_H