#ifndef CREDITSTATE_H
#define CREDITSTATE_H
#include "State.h"
class CreditState : public State
{
private:
	sf::Font _font;
	sf::Text _credit1;
	sf::Text _credit2;
	sf::Text _credit3;
	sf::Text _credit4;
public:
	CreditState();
	~CreditState();
	virtual void UpdateFrame(sf::RenderWindow* window, sf::Time* elapsed);
};
#endif // !CREDITSTATE_H


