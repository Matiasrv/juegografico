#include "Entity.h"

Entity::Entity(float x, float y)
	:GameObject(x, y),
	_lives(0),
	_speed(0)
{
}
void Entity::setLives(int lives)
{
	_lives = lives;
}
int Entity::getLives() const
{
	return _lives;
}