#include "Enemy.h"


Enemy::Enemy(float x, float y) : Entity(x, y)
{
	//_lives = 1;
	_speed = 500.0f;
}
int Enemy::getLives() const
{
	return _lives;
}

void Enemy::setLives(int lives)
{
	_lives = lives;
}

void Enemy::MoveLeft(sf::Time* elapsed)
{
	if (_sprite.getPosition().x > 0)
		_sprite.move(-_speed * (*elapsed).asSeconds(), 0);
}

void Enemy::MoveRight(sf::Time* elapsed)
{
	if (_sprite.getPosition().x < 750)
		_sprite.move(_speed * (*elapsed).asSeconds(), 0);
}

void Enemy::MoveUp(sf::Time* elapsed)
{
	if (_sprite.getPosition().y > 0)
		_sprite.move(0, -_speed * (*elapsed).asSeconds());
}

void Enemy::MoveDown(sf::Time* elapsed)
{
	if (_sprite.getPosition().y < 550)
		_sprite.move(0, _speed * (*elapsed).asSeconds());
}

void Enemy::Update(sf::Time* elapsed)
{
	switch (rand() % 4)
	{
	case 0:
		Enemy::MoveUp(elapsed);
		break;
	case 1:
		Enemy::MoveDown(elapsed);
		break;
	case 2:
		Enemy::MoveLeft(elapsed);
		break;
	default:
		Enemy::MoveRight(elapsed);
		break;
	}
}
