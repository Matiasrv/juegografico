#ifndef INGAMESTATE_H
#define INGAMESTATE_H

#include "State.h"
#include "Player.h"
#include "Enemy.h"
#include "Coin.h"
#include <vector>
#include <time.h>
#include <list>

#define SPAWNSECONDS 3
#define ENEMYNUMBER 5

class InGameState : public State
{
private:
	int _score;
	sf::Font _font;
	sf::Text _textScore;
	Player* _jugador;
	sf::Texture _enemyTexture;
	sf::Texture _playerTexture;
	sf::Texture _coinTexture;
	float timerSpawn;
	list<Coin*> *_coinList = new list<Coin*>;
	list<Coin*>::iterator iterBeginCoin;
	list<Coin*>::iterator iterEndCoin;
	list<Coin*>::iterator iterCoin;
	list<Enemy*> *_enemyList = new list<Enemy*>;
	list<Enemy*>::iterator iterBeginEnemy;
	list<Enemy*>::iterator iterEndEnemy;
	list<Enemy*>::iterator iterEnemy;
	sf::RectangleShape _playerSpawn;
public:
	InGameState();
	~InGameState();
	virtual void UpdateFrame(sf::RenderWindow* window, sf::Time* elapsed);
	void CollitionChecker();
	int GetScore()const;
	void UpdateScore();
};

#endif // !INGAMESTATE_H