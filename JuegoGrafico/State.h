#ifndef STATE_H
#define STATE_H
#include "SFML\Graphics.hpp"

using namespace std;

class State
{
protected:
	string _nameState;
public:
	State();
	virtual ~State();
	virtual void UpdateFrame(sf::RenderWindow* window, sf::Time* elapsed) = 0;
	virtual string GetCurrentState()const;
};

#endif // !STATE_H