#ifndef CLIMA_H
#define	CLIMA_H
#include <iostream>
#include "SFML\Network.hpp"
#include "json.hpp"

using namespace std;
class Clima abstract
{
public:
	static string getWeather(string city);
};
#endif // CLIMA_H