#include "CreditState.h"



CreditState::CreditState()
{
	_nameState = "Credits";

	_font.loadFromFile("Assets/Fonts/orange juice.ttf");

	_credit1.setFont(_font);
	_credit1.setString("Autor");
	_credit1.setCharacterSize(48);
	_credit1.setFillColor(sf::Color::Red);
	_credit1.setStyle(sf::Text::Bold | sf::Text::Underlined);

	_credit2.setFont(_font);
	_credit2.setString("Velazquez Matias");
	_credit2.setCharacterSize(30);
	_credit2.setFillColor(sf::Color::Red);
	_credit2.setStyle(sf::Text::Bold | sf::Text::Underlined);

	_credit3.setFont(_font);
	_credit3.setString("Font: Orange Juice by Brittney Murphy\n http://www.1001fonts.com/");
	_credit3.setCharacterSize(30);
	_credit3.setFillColor(sf::Color::Red);
	_credit3.setStyle(sf::Text::Bold | sf::Text::Underlined);

	_credit4.setFont(_font);
	_credit4.setString("Pumpkin Sprite by Code Inferno Games \n https://opengameart.org/content/pumpkin");
	_credit4.setCharacterSize(30);
	_credit4.setFillColor(sf::Color::Red);
	_credit4.setStyle(sf::Text::Bold | sf::Text::Underlined);
}


CreditState::~CreditState()
{
}

void CreditState::UpdateFrame(sf::RenderWindow * window, sf::Time * elapsed)
{
	sf::Vector2u windowSize = window->getSize();

	_credit1.setPosition(windowSize.x / 2 - _credit1.getGlobalBounds().width / 2, windowSize.y / 4 - _credit1.getGlobalBounds().height / 2);
	window->draw(_credit1);

	_credit2.setPosition(windowSize.x / 2 - _credit2.getGlobalBounds().width / 2, windowSize.y / 3 - _credit2.getGlobalBounds().height / 2);
	window->draw(_credit2);

	_credit3.setPosition(windowSize.x / 2 - _credit3.getGlobalBounds().width / 2, windowSize.y / 2 - _credit3.getGlobalBounds().height / 2);
	window->draw(_credit3);

	_credit4.setPosition(windowSize.x / 2 - _credit4.getGlobalBounds().width / 2, windowSize.y / 1.5 - _credit4.getGlobalBounds().height / 2);
	window->draw(_credit4);
}
