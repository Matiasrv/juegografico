#ifndef COIN_H
#define COIN_H
#include "GameObject.h"

class Coin : public GameObject
{
public:
	Coin(float x, float y);
	~Coin();
};

#endif // !COIN_H