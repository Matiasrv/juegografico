#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SFML\Graphics.hpp>
#include <iostream>
using namespace std;

class GameObject abstract
{
protected:
	sf::Texture _texture;
	sf::Sprite _sprite;
public:
	GameObject(float x, float y);
	sf::Texture getTexture() const;
	sf::Sprite getSprite() const;
	void setPosition(float x, float y);
	void setGraphic(string textureName, float width, float height);
	void setGraphic(sf::Texture texture);
	bool collide(GameObject* gameObject) const;
};

#endif // !GAMEOBJECT_H