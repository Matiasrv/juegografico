#include "GameObject.h"


GameObject::GameObject(float x, float y)
{
	_sprite.setPosition(sf::Vector2f(x, y));
}
sf::Texture GameObject::getTexture() const
{
	return _texture;
}
sf::Sprite GameObject::getSprite() const
{
	return _sprite;
}
void GameObject::setPosition(float x, float y)
{
	_sprite.setPosition(sf::Vector2f(x, y));
}
void GameObject::setGraphic(string textureName, float width, float height)
{
	_texture.loadFromFile(textureName, sf::IntRect(0, 0, width, height));
	_sprite.setTexture(_texture);
}
void GameObject::setGraphic(sf::Texture texture)
{
	_texture = texture;
	_sprite.setTexture(_texture);
}
bool GameObject::collide(GameObject* gameObject) const
{
	if (this->getSprite().getGlobalBounds().intersects(gameObject->getSprite().getGlobalBounds()))
		return true;
	return false;
}