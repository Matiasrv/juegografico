#ifndef ENEMY_H
#define ENEMY_H
#include "Entity.h"

class Enemy : public Entity
{
public:
	Enemy(float x, float y);
	int getLives() const;
	void setLives(int lives);
	virtual void MoveLeft(sf::Time* elapsed);
	virtual void MoveRight(sf::Time* elapsed);
	virtual void MoveUp(sf::Time* elapsed);
	virtual void MoveDown(sf::Time* elapsed);
	void Update(sf::Time* elapsed);
};

#endif //ENEMY_H