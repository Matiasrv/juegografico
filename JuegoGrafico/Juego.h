#ifndef JUEGO_H
#define JUEGO_H

#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include "InGameState.h"
#include "MenuState.h"
#include "CreditState.h"
#if DEBUG
#include "vld.h"
#elif RELEASE 
#include <Windows.h>
#endif

class Juego
{
private:
	sf::RenderWindow _window;
	sf::Clock _clock;
	sf::Music _mainMusic;
	const int _framesPerSecond = 60;
	State* currentState;
public:
	Juego();
	~Juego();
	void Run();
	void StateManager();
	static const int _windowWidth = 800;
	static const int _windowHeight = 600;
};

#endif //JUEGO_H