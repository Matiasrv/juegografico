#include "InGameState.h"

InGameState::InGameState() :_score(0), timerSpawn(0)
{
	_nameState = "inGame";

	_font.loadFromFile("Assets/Fonts/orange juice.ttf");

	_textScore.setFont(_font);
	_textScore.setString("" + _score);
	_textScore.setCharacterSize(25);
	_textScore.setFillColor(sf::Color::Red);
	_textScore.setStyle(sf::Text::Bold | sf::Text::Underlined);
	_textScore.setPosition(500, 0);


	_jugador = new Player(400, 300);

	_enemyTexture;
	_enemyTexture.loadFromFile("Assets/Textures/pumpkin.png", sf::IntRect(0, 0, 64, 64));
	_playerTexture;
	_playerTexture.loadFromFile("Assets/Textures/pumpkin.png", sf::IntRect(0, 0, 64, 64));
	_coinTexture;
	_coinTexture.loadFromFile("Assets/Textures/pumpkin.png", sf::IntRect(0, 0, 64, 64));
	for (int i = 0; i < ENEMYNUMBER; i++)
	{
		_enemyList->push_back(new Enemy(rand() % 750, rand() % 550));
	}
	iterBeginEnemy = _enemyList->begin();
	iterEndEnemy = _enemyList->end();
	for (iterEnemy = iterBeginEnemy; iterEnemy != iterEndEnemy; iterEnemy++)
	{
		(*iterEnemy)->setGraphic(_enemyTexture);
	}
	_jugador->setGraphic(_playerTexture);
}


InGameState::~InGameState()
{
	for (iterEnemy = iterBeginEnemy; iterEnemy != iterEndEnemy; iterEnemy++)
	{
		delete (*iterEnemy);
	}
	iterBeginCoin = _coinList->begin();
	iterEndCoin = _coinList->end();
	for (iterCoin = iterBeginCoin; iterCoin != iterEndCoin; iterCoin++)
	{
		delete (*iterCoin);
	}
	delete _coinList;
	delete _enemyList;
	if (_jugador)
	{
		delete _jugador;
		_jugador = NULL;
	}
}

void InGameState::UpdateFrame(sf::RenderWindow* window, sf::Time* elapsed)
{
	CollitionChecker();

	timerSpawn += 1 * elapsed->asSeconds();
	cout << timerSpawn << endl;
	if (timerSpawn >= SPAWNSECONDS)
	{
		timerSpawn = 0;
		Coin* tempCoin = new Coin(rand() % 750, rand() % 550);
		tempCoin->setGraphic(_coinTexture);
		_coinList->push_back(tempCoin);
	}
	iterBeginCoin = _coinList->begin();
	iterEndCoin = _coinList->end();
	for (iterCoin = iterBeginCoin; iterCoin != iterEndCoin; iterCoin++)
	{
		if ((*iterCoin))
			window->draw((*iterCoin)->getSprite());
	}
	for (iterEnemy = iterBeginEnemy; iterEnemy != iterEndEnemy; iterEnemy++)
	{
		if ((*iterEnemy))
		{
			(*iterEnemy)->Update(elapsed);
			window->draw((*iterEnemy)->getSprite());
		}
	}
	if (_jugador)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			_jugador->MoveLeft(elapsed);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			_jugador->MoveRight(elapsed);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			_jugador->MoveUp(elapsed);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			_jugador->MoveDown(elapsed);

		window->draw(_jugador->getSprite());

	}
	/*else
		mostrar texto gameover*/

	UpdateScore();
	window->draw(_textScore);
}

void InGameState::CollitionChecker()
{
	for (iterEnemy = iterBeginEnemy; iterEnemy != iterEndEnemy; iterEnemy++)
	{
		if ((*iterEnemy) && _jugador)
		{
			if ((*iterEnemy)->collide(_jugador))
			{
				_jugador->setPosition(_jugador->startingPosition.x, _jugador->startingPosition.y);
				_jugador->setLives(_jugador->getLives() - 1);
				if (_jugador->getLives() == 0)
				{
					delete _jugador;
					_jugador = NULL;
				}
			}
		}
	}
	iterBeginCoin = _coinList->begin();
	iterEndCoin = _coinList->end();
	for (iterCoin = iterBeginCoin; iterCoin != iterEndCoin; iterCoin++)
	{
		if ((*iterCoin) && _jugador)
		{
			if ((*iterCoin)->collide(_jugador))
			{
				delete (*iterCoin);
				(*iterCoin) = NULL;
				_score++;
			}
		}
	}
}

int InGameState::GetScore() const
{
	return _score;
}

void InGameState::UpdateScore()
{
	_textScore.setString("SCORE " + to_string(_score));
}
